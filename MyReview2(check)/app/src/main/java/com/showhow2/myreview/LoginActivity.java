package com.showhow2.myreview;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.showhow2.myreview.util.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class LoginActivity extends ActionBarActivity {

    private EditText Id;
    private EditText Pwd;

    private ProgressDialog pDialog;
    //String response = null;
   // private LoginAsyncTask loginAsyncTask;


    JSONParser jsonParser = new JSONParser();
    private static final String LOGIN_URL = "http://192.168.1.182/~abhijeet/showhow2/jsons/webservices/loginauth?app=2A192A0C22";
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        Id = (EditText) findViewById(R.id.email);
        Pwd = (EditText) findViewById(R.id.password);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void doLogin(View view) {

        new AttemptLogin().execute();

    }




    private class AttemptLogin extends AsyncTask<String, String, String> {


        boolean failure = false;

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            pDialog = new ProgressDialog(LoginActivity.this);
            pDialog.setMessage("Attempting for login...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }



        @Override
        protected String doInBackground(String... args) {

            int success;

            String id=Id.getText().toString();
            String pwd=Pwd.getText().toString();


               List<NameValuePair> params = new ArrayList<>();

               params.add(new BasicNameValuePair("id", id));
               params.add(new BasicNameValuePair("pwd", pwd));

               Log.d("request!", "starting");
               JSONObject json = jsonParser.makeHttpRequest( LOGIN_URL, "POST", params);


                Log.d("Login attempt", json.toString());
            try{
               success = json.getInt(TAG_SUCCESS);
               if (success == 1){

                  Log.d("Successfully Login!", json.toString());

                   Intent intent = new Intent(LoginActivity.this, MyReviewActivity.class);
                   finish();
                   startActivity(intent);

                   return  json.getString(TAG_MESSAGE);
               }else{
                   Log.d("Login Failed!",json.getString(TAG_MESSAGE));
                   return  json.getString(TAG_MESSAGE);

               }
           }catch (JSONException e){
               e.printStackTrace();
           }
            return  null;
           }

        protected  void onPostExecute(String message){

            pDialog.dismiss();
            if (message != null){

                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

/**
 * Async Task to check whether internet connection is working.
 **/






public void doSignUp(View view) {

        this.startActivity(new Intent(this, SignupActivity.class));
    }
}
