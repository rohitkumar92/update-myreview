package com.showhow2.myreview.util;

/**
 * Created by Rohit-PC on 07-02-2015.
 */
public class ConfigHelper {
    public static final String REGISTER_URL="";
    public static final String LOGIN_URL="http://192.168.1.182/~abhijeet/showhow2/jsons/webservices/loginauth?app=2A192A0C22";



    public static String parseMessageFromCode(int code){
        switch(code){
            case 1:
                return "Invalid User name or Password";
            case 2:
                return "Username should be of 4 characters";
            case 3:
                return "Username already taken";
            case 4:
                return "Username must contains only alphabets or numbers";
            case 5:
                return "Password must be at least 8 characters long";
            case 6:
                return "Password and Confirm Password do not match";
            case 7:
                return "Not a Valid email id";
            case 8:
                return "This email ID already exists";
            case 9:
                return "First name cannot be blank";
            case 10:
                return "Invalid Date";
            case 11:
                return "ISBN number must be numeric and of 10 or 13 characters";
            case 400:
            case 404:
            case 500:
                return "Oops something went wrong, please try in some time";
        }
        return "";
    }
}
