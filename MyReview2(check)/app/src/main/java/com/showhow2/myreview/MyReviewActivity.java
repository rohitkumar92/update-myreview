package com.showhow2.myreview;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.showhow2.myreview.backend.beans.ReviewBean;
import com.showhow2.myreview.util.DBHelper;

import java.util.ArrayList;
import java.util.List;

public class MyReviewActivity extends ActionBarActivity {

    /*private ListView list;
    private List<String> displayList;
    private DBHelper mydb;
    private ArrayAdapter<String> arrayAdapter;
    List<ReviewBean> arrayList;*/

    private ListView list;
    private List<String> displayList;
    private List<ReviewBean> arrayList;
    private DBHelper mydb;
    private ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myreviews);

        mydb = new DBHelper(this);
        arrayList = mydb.getAllReviews();
        displayList = new ArrayList<>();

        for (ReviewBean reviewBean : arrayList) {
            displayList.add(reviewBean.getName());
        }

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, displayList);

        list = (ListView) findViewById(R.id.review);
        list.setAdapter(arrayAdapter);

        list.setOnItemLongClickListener(new OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                removeItemFromList(position);
                return false;
            }
        });

    }

    private void removeItemFromList(int position) {

        final int deletePosition = position;

        AlertDialog.Builder alert = new AlertDialog.Builder(MyReviewActivity.this);
        alert.setTitle("Delete");
        alert.setMessage("Do You Want To Delete?");
                alert.setPositiveButton("YES", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                displayList.remove(deletePosition);
                arrayAdapter.notifyDataSetChanged();
                arrayAdapter.notifyDataSetInvalidated();

            }
        });

        alert.setNegativeButton("CANCEL", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_landing, menu);




        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_add:
                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id", 0);
                Intent intent = new Intent(getApplicationContext(), AddReviewsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                Intent intent1 = new Intent(MyReviewActivity.this, LoginActivity.class);
                startActivity(intent1);
                return true;
            case R.id.action_share:







                Intent shareIntent =  new Intent(Intent.ACTION_SEND);
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "text to share");

                startActivity(shareIntent);


                return true;

           /* case R.id.action_delete:
                String text = "";
                List<ReviewBean> selectedReviews = new ArrayList<>();
                int count = list.getCount();
                SparseBooleanArray items = list.getCheckedItemPositions();

                for (int i = 0; i < count; i++) {
                    if (items.get(i)) {
                        text += this.reviews.get(i).getName() + ",";
                        selectedReviews.add(this.reviews.get(i));
                    }
                }
                Toast.makeText(this, text, Toast.LENGTH_SHORT).show();*/


            default:
                return super.onOptionsItemSelected(item);


        }

    }

    @Override
    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
        }
        return super.onKeyDown(keycode, event);
    }

}




