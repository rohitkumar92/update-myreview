package com.showhow2.myreview;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.showhow2.myreview.backend.beans.ReviewBean;

import java.util.List;

/**
 * Created by Rohit-PC on 03-02-2015.
 */
public class ListViewAdapter extends ArrayAdapter<ReviewBean> {

    //private ReviewBean reviewBean;
    List<ReviewBean> values;
    Context context;
    private SparseBooleanArray mSelectedItems;//newly added
    LayoutInflater inflater;//newly added

    public ListViewAdapter(Context context, int resourceId, List<ReviewBean> values) {//previously item was there
        super(context, resourceId, values);
        mSelectedItems = new SparseBooleanArray();//newly added
        this.context = context;
        this.values = values;
        inflater = LayoutInflater.from(context);//newly added
    }




    private class ViewHolder{
        TextView title;
        TextView category;
       // VideoView video;
    }

    public View getView(int position, View view, ViewGroup parent){

        final ViewHolder holder;
        if(view==null){

            holder=new ViewHolder();
            view = inflater.inflate(R.layout.activity_myreviews, null);

            holder.title=(TextView)view.findViewById(R.id.title);
            holder.category=(TextView)view.findViewById(R.id.category);
           // holder.video=(VideoView)view.findViewById(R.id.start);
            view.setTag(holder);
        }else {

            holder = (ViewHolder)view.getTag();

        }
        holder.title.setText(values.get(position).getName());
        holder.category.setText(values.get(position).getCategory());




        return view;

    }

  /*  public void remove(ReviewBean object){

        values.remove(object);
        notifyDataSetChanged();

    }*/
   /* public List<ReviewBean> getReviewBean(){

        return values;

    }*/

    public void toggleSelection(int position){

        selectView(position, !mSelectedItems.get(position));

    }

    public void removeSelection(){

        mSelectedItems = new SparseBooleanArray();
        notifyDataSetChanged();

    }

    public void selectView(int position, boolean value){


        if(value)
            mSelectedItems.put(position, value);
        else
            mSelectedItems.delete(position);
        notifyDataSetChanged();

    }

    public int getSelectedCount(){
        return mSelectedItems.size();
    }

    public SparseBooleanArray getSelectedItems(){

        return mSelectedItems;
    }


/*
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_myreviews, parent, false);
        TextView title = (TextView) rowView.findViewById(R.id.title);
        TextView category = (TextView) rowView.findViewById(R.id.category);
        VideoView videoView = (VideoView) rowView.findViewById(R.id.start);

        ReviewBean reviewBean = this.values.get(position);

        title.setText(reviewBean.getName());
        category.setText(reviewBean.getCategory());
        videoView.setVideoURI(Uri.parse(reviewBean.getFileName()));

        return rowView;
    }*/
}
