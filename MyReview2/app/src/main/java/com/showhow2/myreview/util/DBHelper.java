package com.showhow2.myreview.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.showhow2.myreview.backend.beans.ReviewBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";
    public static final String REVIEW_TABLE_NAME = "reviews";
    public static final String REVIEW_COLUMN_ID = "id";
    public static final String REVIEW_COLUMN_NAME = "name";
    public static final String REVIEW_COLUMN_CATEGORY = "category";
    public static final String REVIEW_COLUMN_FILENAME = "filename";


    //private HashMap hp;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table reviews " +
                        "(id integer primary key autoincrement, name varchar(200),category varchar(200),filename varchar(200))"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS reviews");
        onCreate(db);
    }

    public boolean insert(ReviewBean reviewBean) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(REVIEW_COLUMN_NAME, reviewBean.getName());
        contentValues.put(REVIEW_COLUMN_CATEGORY, reviewBean.getCategory());
        contentValues.put(REVIEW_COLUMN_FILENAME, reviewBean.getFileName());

        db.insert(REVIEW_TABLE_NAME, null, contentValues);

        db.close();
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from reviews where id=" + id + "", null);
        return res;
    }

    public ReviewBean getReview(int id) {
        Cursor cursor = this.getData(id);
        ReviewBean reviewBean = new ReviewBean();
        cursor.moveToFirst();

        reviewBean.setId(id);
        reviewBean.setName(cursor.getString(cursor.getColumnIndex(DBHelper.REVIEW_COLUMN_NAME)));
        reviewBean.setCategory(cursor.getString(cursor.getColumnIndex(DBHelper.REVIEW_COLUMN_CATEGORY)));
        reviewBean.setFileName(cursor.getString(cursor.getColumnIndex(DBHelper.REVIEW_COLUMN_FILENAME)));

        if (!cursor.isClosed()) {
            cursor.close();
        }
        return reviewBean;
    }

    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, REVIEW_TABLE_NAME);
        return numRows;
    }

    public boolean update(ReviewBean reviewBean) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(REVIEW_COLUMN_NAME, reviewBean.getName());
        contentValues.put(REVIEW_COLUMN_CATEGORY, reviewBean.getCategory());
        contentValues.put(REVIEW_COLUMN_FILENAME, reviewBean.getFileName());

        db.update(REVIEW_TABLE_NAME, contentValues, "id = ? ", new String[]{Integer.toString(reviewBean.getId())});
        return true;
    }

    public int delete(ReviewBean reviewBean) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(REVIEW_TABLE_NAME,
                "id = ? ",
                new String[]{Integer.toString(reviewBean.getId())});
    }

    public List<ReviewBean> getAllReviews() {

        List<ReviewBean> results = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from reviews", null);
        cursor.moveToFirst();

        while (cursor.isAfterLast() == false) {

            ReviewBean reviewBean = new ReviewBean();

            reviewBean.setId(cursor.getInt(cursor.getColumnIndex(DBHelper.REVIEW_COLUMN_ID)));
            reviewBean.setName(cursor.getString(cursor.getColumnIndex(DBHelper.REVIEW_COLUMN_NAME)));
            reviewBean.setCategory(cursor.getString(cursor.getColumnIndex(DBHelper.REVIEW_COLUMN_CATEGORY)));
            reviewBean.setFileName(cursor.getString(cursor.getColumnIndex(DBHelper.REVIEW_COLUMN_FILENAME)));

            results.add(reviewBean);

            cursor.moveToNext();
        }
        return results;
    }
    public HashMap<String, String> getUserDetails(){
        HashMap<String,String> user = new HashMap<String,String>();
        String selectQuery = "SELECT  * FROM " + REVIEW_TABLE_NAME;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        // Move to first row
        cursor.moveToFirst();
        if(cursor.getCount() > 0){
            user.put("email", cursor.getString(1));
            user.put("lname", cursor.getString(2));
            user.put("email", cursor.getString(3));
            user.put("uname", cursor.getString(4));
            user.put("uid", cursor.getString(5));
            user.put("created_at", cursor.getString(6));
        }
        cursor.close();
        db.close();
        // return user
        return user;
    }
}