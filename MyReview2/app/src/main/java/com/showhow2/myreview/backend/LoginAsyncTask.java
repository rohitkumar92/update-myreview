package com.showhow2.myreview.backend;

import android.os.AsyncTask;
import android.util.Log;

import com.showhow2.myreview.BuildConfig;
import com.showhow2.myreview.util.ConfigHelper;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rohit-PC on 07-02-2015.
 */
public class LoginAsyncTask extends AsyncTask<String, Void, JSONObject> {


    @Override
    protected JSONObject doInBackground(String... params) {
        String emailId = params[0];
        String password = params[1];


        //Create a new HttpClient and Post Header
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(ConfigHelper.LOGIN_URL);

        if (BuildConfig.DEBUG) Log.i(ConfigHelper.LOGIN_URL, "Not Passed");



        try{

            List<NameValuePair> nameValuePairs = new ArrayList<>();

            nameValuePairs.add(new BasicNameValuePair("data[User][emailId]", emailId));
            nameValuePairs.add(new BasicNameValuePair("data[User][password]", password));
            httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpClient.execute(httpPost);
            String responseText = EntityUtils.toString(response.getEntity());

            JSONObject json = new JSONObject(responseText);

            Log.e("response", responseText);

            return json;

        } catch (ClientProtocolException e) {
            Log.e("error",e.toString());
        } catch (IOException e) {
            Log.e("error",e.toString());
        } catch (JSONException e){
            Log.e("error",e.toString());
        }
        return null;
    }

    /*public JSONObject loginUser(String email, String password){
        // Building Parameters
        List<NameValuePair> params = new ArrayList<>();

        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("password", password));


        JSONObject json = new JSONObject(responseText);

        Log.e("response", responseText);

        return json;

    }*/
}