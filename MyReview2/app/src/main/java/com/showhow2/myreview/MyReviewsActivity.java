package com.showhow2.myreview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.showhow2.myreview.backend.beans.ReviewBean;
import com.showhow2.myreview.util.DBHelper;

import java.util.ArrayList;
import java.util.List;

public class MyReviewsActivity extends ActionBarActivity {

    //public final static String EXTRA_MESSAGE = "com.showhow2.myreview.MESSAGE";
    ListView list;
    ListViewAdapter listViewAdapter;
    List<ReviewBean> reviews = new ArrayList<ReviewBean>();
    List<String> displayList;
    DBHelper mydb;


  /*  List<String> displayList;
    List<ReviewBean> arrayList;
    DBHelper mydb;
    private ReviewBean reviewBean;*/

    //private ReviewBean reviewBean;

    ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myreviews);

        mydb = new DBHelper(this);
        reviews = mydb.getAllReviews();
        displayList = new ArrayList<>();

        for (ReviewBean reviewBean : reviews) {
            displayList.add(reviewBean.getName());
        }

        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, displayList);
        list = (ListView)findViewById(R.id.reviews);
        list.setAdapter(arrayAdapter);


        listViewAdapter = new ListViewAdapter(this,android.R.layout.simple_list_item_1, reviews);
        //list.setAdapter(listViewAdapter);
        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);




        list.setMultiChoiceModeListener(new MultiChoiceModeListener() {

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

                final int checkedCount = list.getCheckedItemCount();
                mode.setTitle(checkedCount + " Selected");
                //listViewAdapter.toggleSelection(position);

            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

                switch (item.getItemId()){

                    case R.id.action_delete:

                        SparseBooleanArray selected = listViewAdapter.getmSelectedItems();

                        for (int i = selected.size()-1;i>=0;i--){

                            if (selected.valueAt(i)){

                                ReviewBean selectedItem = listViewAdapter.getItem(selected.keyAt(i));
                                listViewAdapter.remove(selectedItem);

                            }

                        }
                        mode.finish();
                        return true;
                    default:
                        return false;
                }

            }



            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
               mode.getMenuInflater().inflate(R.menu.menu_landing, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }



            @Override
            public void onDestroyActionMode(ActionMode mode) {

                listViewAdapter.removeSelection();

            }
        });
        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                int id = arg2 + 1;
                Intent intent = new Intent(getApplicationContext(), AddReviewsActivity.class);
                intent.putExtra("id", id);

                startActivity(intent);
            }
        });
        /*this.reviewBean=new ReviewBean();

        title.setText(this.reviewBean.getName());
        category.setText(this.reviewBean.getCategory());



        for (int i = 0;i< title.length();i++){

            ReviewBean review = new ReviewBean(title[i],category[i]);
            reviews.add(review);

        }*/
       /* mydb = new DBHelper(this);
        arrayList = mydb.getAllReviews();
        displayList = new ArrayList<>();

        for (ReviewBean reviewBean : arrayList) {
            displayList.add(reviewBean.getName());
        }


        arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, displayList);

        list = (ListView) findViewById(R.id.reviews);
        list.setAdapter(arrayAdapter);

        list.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                removeItemFromList(position);

                return true;
            }


        });


        list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                int id = arg2 + 1;
                Intent intent = new Intent(getApplicationContext(), AddReviewsActivity.class);
                intent.putExtra("id", id);

                startActivity(intent);
            }
        });*/
    }
   /* public void removeItemFromList(int position) {

        final int deletePosition = position;

        AlertDialog.Builder alert = new AlertDialog.Builder(MyReviewsActivity.this);
        alert.setTitle("Delete");
        alert.setMessage("Do You Want To Delte");
        alert.setPositiveButton("Yes", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                displayList.remove(deletePosition);
                arrayAdapter.notifyDataSetChanged();
                arrayAdapter.notifyDataSetInvalidated();

            }
        });

        alert.setNegativeButton("CANCEL", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();*/




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_landing, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {

            case R.id.action_add:
                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id", 0);
                Intent intent = new Intent(getApplicationContext(), AddReviewsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_logout:
                Intent intent1 = new Intent(MyReviewsActivity.this, LoginActivity.class);
                startActivity(intent1);
                return  true;

            //case R.id.action_delete:

            /*case R.id.action_delete:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.delete_review)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mydb.delete(reviewBean);
                                Toast.makeText(getApplicationContext(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MyReviewsActivity.class);
                                startActivity(intent);
                            }
                        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                AlertDialog d = builder.create();
                d.setTitle("Are you sure");
                d.show();
                break;*/
            default:
                return super.onOptionsItemSelected(item);


        }

    }


    public boolean onKeyDown(int keycode, KeyEvent event) {
        if (keycode == KeyEvent.KEYCODE_BACK) {
            moveTaskToBack(true);
        }
        return super.onKeyDown(keycode, event);
    }


    }




