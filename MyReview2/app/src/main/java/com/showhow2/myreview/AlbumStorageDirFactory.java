package com.showhow2.myreview;

import java.io.File;

/**
 * Created by Rohit-PC on 28-01-2015.
 */
abstract class AlbumStorageDirFactory {

    public abstract File getAlbumStorageDir(String albumName);


}
