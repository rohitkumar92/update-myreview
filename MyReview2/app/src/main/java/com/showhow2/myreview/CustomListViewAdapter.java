package com.showhow2.myreview;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.VideoView;

import com.showhow2.myreview.backend.beans.ReviewBean;

import java.util.List;

/**
 * Created by Rohit-PC on 03-02-2015.
 */
public class CustomListViewAdapter extends ArrayAdapter<ReviewBean> {

    //private ReviewBean reviewBean;
    private List<ReviewBean> values;
    private Context context;

    public CustomListViewAdapter(Context context, int resourceId, List<ReviewBean> items) {
        super(context, resourceId, items);
        this.context = context;
        this.values = items;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.activity_myreviews, parent, false);
        TextView title = (TextView) rowView.findViewById(R.id.title);
        TextView category = (TextView) rowView.findViewById(R.id.category);
        VideoView videoView = (VideoView) rowView.findViewById(R.id.start);

        ReviewBean reviewBean = this.values.get(position);

        title.setText(reviewBean.getName());
        category.setText(reviewBean.getCategory());
        videoView.setVideoURI(Uri.parse(reviewBean.getFileName()));

        return rowView;
    }
}
