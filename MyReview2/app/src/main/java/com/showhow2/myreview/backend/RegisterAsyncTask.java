package com.showhow2.myreview.backend;

import android.os.AsyncTask;
import android.util.Log;

import com.showhow2.myreview.util.ConfigHelper;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rohit-PC on 07-02-2015.
 */
public class RegisterAsyncTask extends AsyncTask<String, Void, JSONObject> {
    @Override
    protected JSONObject doInBackground(String... params) {

        String name = params[0];
        String password = params[1];
        String confirm = params[2];
        String email = params[3];
// Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(ConfigHelper.REGISTER_URL);

        try {
            // Add your data
            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("data[User][username]", name));
            nameValuePairs.add(new BasicNameValuePair("data[User][password]", password));
            nameValuePairs.add(new BasicNameValuePair("data[User][confirmpassword]", confirm));
            nameValuePairs.add(new BasicNameValuePair("data[User][email]", email));
            nameValuePairs.add(new BasicNameValuePair("data[User][group_id]", "2"));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httppost);
            String responseText = EntityUtils.toString(response.getEntity());

            JSONObject json = new JSONObject(responseText);
            Log.e("response", responseText);
            return json;

        } catch (ClientProtocolException e) {
            Log.e("error",e.toString());
        } catch (IOException e) {
            Log.e("error",e.toString());
        } catch (JSONException e){
            Log.e("error",e.toString());
        }
        return null;
    }
}
