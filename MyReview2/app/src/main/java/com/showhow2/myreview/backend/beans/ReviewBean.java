package com.showhow2.myreview.backend.beans;

/**
 * This bean represents the reviews
 * Created by Rohit-PC on 31-01-2015.
 */
public class ReviewBean {
    private int id;
    private String name;
    private String category;
    private String fileName;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
