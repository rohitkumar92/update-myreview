package com.showhow2.myreview;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.showhow2.myreview.backend.beans.ReviewBean;
import com.showhow2.myreview.util.DBHelper;


public class AddReviewsActivity extends ActionBarActivity {

    private static final int REQUEST_VIDEO_CAPTURE = 1;

    private DBHelper mydb;
    private TextView title;
    private TextView category;
    private VideoView videoView;
    private Uri videoUri;
    private Bundle extras;
    private ReviewBean reviewBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_review);



        this.reviewBean=new ReviewBean();
        this.reviewBean.setId(0);

        title = (TextView) findViewById(R.id.title);
        category = (TextView) findViewById(R.id.category);
        this.videoView = (VideoView) this.findViewById(R.id.start);

        mydb = new DBHelper(this);

        this.extras = getIntent().getExtras();

        if (extras != null) {
            int id = extras.getInt("id");

            if (id > 0) {

                this.reviewBean = mydb.getReview(id);

                title.setText(this.reviewBean.getName());
                category.setText(this.reviewBean.getCategory());
                this.videoUri=Uri.parse(this.reviewBean.getFileName());
                videoView.setVideoURI(this.videoUri);
                videoView.start();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_video, menu);
        /*Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int Value = extras.getInt("id");
            if (Value > 0) {
                getMenuInflater().inflate(R.menu.menu_add_video, menu);
            } else {
                getMenuInflater().inflate(R.menu.menu_landing, menu);
            }
        }*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch (item.getItemId()) {

            case R.id.action_save:

                this.reviewBean.setName(title.getText().toString());
                this.reviewBean.setCategory(category.getText().toString());
                this.reviewBean.setFileName(this.videoUri.toString());

                if (this.reviewBean.getId() > 0) {
                    Log.e("Rohit", "Update");
                    if (mydb.update(this.reviewBean)) {
                        Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getApplicationContext(), MyReviewActivity.class));
                    } else {
                        Toast.makeText(getApplicationContext(), "not Updated", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.e("Rohit", "Insert");
                    if (mydb.insert(this.reviewBean)) {
                        Toast.makeText(getApplicationContext(), "done", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "not done", Toast.LENGTH_SHORT).show();
                    }
                    startActivity(new Intent(this, MyReviewActivity.class));
                }
                break;
            case R.id.action_cancel:
                this.startActivity(new Intent(this, MyReviewActivity.class));


                //case R.id.action_edit:
                /*Button b = (Button) findViewById(R.id.done);
                b.setVisibility(View.VISIBLE);

                title.setEnabled(true);
                title.setFocusableInTouchMode(true);
                title.setClickable(true);

                category.setEnabled(true);
                category.setFocusableInTouchMode(true);
                category.setClickable(true);*/

                // return true;

           /* case R.id.action_del:

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(R.string.delete_review)
                        .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                mydb.delete(reviewBean);
                                Toast.makeText(getApplicationContext(), "Deleted Successfully", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(getApplicationContext(), MyReviewsActivity.class);
                                startActivity(intent);
                            }
                        }).setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
                AlertDialog d = builder.create();
                d.setTitle("Are you sure");
                d.show();

                break;*/
            default:
                return super.onOptionsItemSelected(item);

        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_VIDEO_CAPTURE && resultCode == RESULT_OK) {
            this.videoUri = data.getData();
            VideoView videoView = (VideoView) this.findViewById(R.id.start);
            videoView.setVideoURI(this.videoUri);
            videoView.start();
            //Log.e("Rohit", this.videoUri.toString());
        }
    }

   /* public void doSave(View view) {

        this.reviewBean.setName(title.getText().toString());
        this.reviewBean.setCategory(category.getText().toString());
        this.reviewBean.setFileName(this.videoUri.toString());

        if (this.reviewBean.getId() > 0) {
            Log.e("Rohit", "Update");
            if (mydb.update(this.reviewBean)) {
                Toast.makeText(getApplicationContext(), "Updated", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), MyReviewsActivity.class));
            } else {
                Toast.makeText(getApplicationContext(), "not Updated", Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.e("Rohit", "Insert");
            if (mydb.insert(this.reviewBean)) {
                Toast.makeText(getApplicationContext(), "done", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "not done", Toast.LENGTH_SHORT).show();
            }
            startActivity(new Intent(this, MyReviewsActivity.class));
        }
    }*/


    public void doCapture(View view) {
        Intent takeVideo = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (takeVideo.resolveActivity(this.getPackageManager()) != null) {
            this.startActivityForResult(takeVideo, REQUEST_VIDEO_CAPTURE);
        }
        //this.startActivity(new Intent(this, CaptureVideoActivity.class));
    }

    public void doStart(View view){
        videoView.start();
    }
    public void doStop(View view){
        videoView.stopPlayback();
    }


   /* public void doCancelVideo(View view) {
        this.startActivity(new Intent(this, MyReviewsActivity.class));
    }
*/
}